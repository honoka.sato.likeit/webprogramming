<%@ page language="java" contentType="text/html:charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
    <h1>ログイン画面</h1>
	 <form class="form-signin" action="LoginServlet" method="post">
                <div class="form-group row">
			<div class="col-sm-2""right">
				<label for="exampleInputEmail1">ログインID</label>
			</div>
			<div class="col-sm-2">
				<input type="email" class="form-control" id="exampleInputEmail1"
					aria-describedby="emailHelp">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2">
				<label for="exampleInputPassword1">パスワード</label>
			</div>
			<div class="col-sm-2">
				<input type="password" class="form-control" id="exampleInputPassword1">
			</div>
		</div>
        <center>
			<div class="col-sm-6">
				<button type="submit" class="btn btn-primary">ログイン</button>
			</div>
        </center>
	</form>

</body>
</html>