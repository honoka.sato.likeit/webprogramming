package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.User;


public class UserDao {
	 public User findByLoginInfo(String loginId, String password) {
		 Connection conn = null;
	        try {
	            conn = DBManager.getConnection();
	            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, loginId);
	            pStmt.setString(2, password);
	          	ResultSet rs = pStmt.executeQuery();


	          	if (!rs.next()) {
	          		return null;
	          		}
	          		String loginIdData = rs.getString("login_id");
	          		String nameData = rs.getString("name");
	          		return new User(loginIdData, nameData);


	        } catch (SQLException e) {
	        	e.printStackTrace();
	} finally {
	 // データベース切断
	          if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	            	}
	        	}
	}
	}


	}

